
<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img alt="Farris UI Logo" src="./farris_design.jpg"  style="max-width:50%;">
  </a>
</p>

<h1 align="center">Farris Vue</h1>

<p align="center">Farris Vue, a Farris Design based Vue3 component library.</p>

English | [Simplified Chinese](README.md)

Farris Design Priciples：

- <b>Fa</b>st :Farris Vue, composed by UI suites with super-fast user experience. Farris Data Grid, with high-speed rendering performance when rendering huge amounts of data.
- <b>R</b>eliable: Provide users with steady and reliable use embodiment and optimize various abnormal interaction scenarios targeted.
- <b>R</b>esponsive: Enhanced responsive design, provides more nuanced responsive interactions inside components.
- <b>I</b>ntuitive: Intuitive design, enables users to have an intuitive interface interaction experience.
- <b>S</b>mart : Intelligent UI, meets the development of intelligent application scenarios.

## 1. Learn about Farris

Visit our [offical site(https://farris-design.gitee.io/farris-vue)](https://farris-design.gitee.io/farris-vue/) to learn more about Farris UI Vue Components.

## 2. Guide to Run a Project Locally

### 2.1 Environment Check

Please execute the following command to check if `yarn` has been installed in the environment before running the project locally.

```
yarn -v
```

If you don't know the version information of `yarn`，please refer to [install yarn](https://yarnpkg.com/getting-started/install).

### 2.2 Get the Source Code

To get the source code and install dependent components, you can do so by typing:
```
npm install lerna -g
git clone https://gitee.com/ubml/farris-vue.git
cd farris-vue
lerna bootstrap
```

### 2.3 Run Sample Site

Run the following commands:

```
cd packages/ui-vue
yarn run docs:dev
```

Open browser to review the sample page：`http://localhost:5173/`。

![sample page](./farris_home_page.png)

### 2.4 Visit Sample Site

You can visit the sample site：

1. Click「quick start」to learn how to install and use Farris Vue.
2. Click「components」to experience component visual effects and interaction effects, and view the component API documentation simultaneously.

## 3. Guide to Use Farris UI Vue

### 3.1 Install @farris/ui-vue

```
npm install @farris/ui-vue
```

```
yarn add @farris/ui-vue
```

### 3.2 Import Farris Vue

Locate your `main.ts` document and import `@farris/ui-vue` into the Application.

```ts
import { createApp } from 'vue';
import App from './App.vue';
import Farris from '@farris/ui-vue';

createApp(App).use(Farris).mount('#app');
```

### 3.3 Use Farris Vue

Once imported, you can attempt to use Farris Vue components. Locate to the `App.vue` document and have a try!

```vue
<template>
    <f-button-edit></f-button-edit>
</template>
```

## 4. How to Be a Farris Vue Contributor

Contribute to Farris Vue.
You can participate in the following contributions:

-   Develop Farris Vue Components with `TypeScript` + `TSX` + `SystemJs` + `Rollup` technologies
-   Contribute to the Farris Theme tool
-   Contribute to the Farris development documentation

If you decide to contribute to Farris UI Vue，you could start from reading our [Contributor guide](./CONTRIBUTING.md).

## ✨ Contributors

Thanks to the following Farris Vue contributors.

<table>
  <!-- <tr>
    <td align="center"><a href="https://juejin.cn/user/712139267650141"><img src="https://foruda.gitee.com/avatar/1662181279170802242/610337_chenshj_1662181279.png!avatar60" width="100px;" alt=""/><br /><sub><b>陈圣杰</b></sub></a><br /></td>
  </tr> -->
</table>

## Open source license

[Apache License 2.0](https://gitee.com/ubml/farris-vue/blob/master/LICENSE)
