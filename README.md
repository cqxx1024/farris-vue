<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img alt="Farris UI Logo" src="./farris_design.jpg"  style="max-width:50%;">
  </a>
</p>

<h1 align="center">Farris Vue</h1>

<p align="center">Farris Vue 是一套基于Farris Design</a> 的 Vue3 组件库。</p>

[English](README.en.md) | 简体中文

Farris 设计原则：

-   <b>Fa</b>st : Farris Vue 是一套具有急速用户体验的 UI 套件，Farris Data Grid 在渲染大量数据时具有高速渲染性能
-   <b>R</b>eliable : 我们为用户提供可靠的使用体现，对各类异常交互场景做出针对性的优化
-   <b>R</b>esponsive : 增强的响应式设计，在组件内部提供更细腻的响应式交互
-   <b>I</b>ntuitive : 采用直觉化的设计，向用户更准确的传达界面交互
-   <b>S</b>mart : 智能化的 UI，满足开发智能应用的场景

## 1. 了解 Farris

可以访问我们的[官方站点(https://farris-design.gitee.io/farris-vue)](https://farris-design.gitee.io/farris-vue/)了解 Farris UI Vue 组件。

## 2. 如何本地运行项目

### 2.1 检查环境

在本地运行项目前，请先执行以下命令，检查环境中是否已经安装 yarn。

```
yarn -v
```

如果未得到`yarn`版本信息，请参考[安装 yarn](https://yarnpkg.com/getting-started/install).

### 2.2 获取源代码

执行以下命令，获取项目源代码，并安装依赖组件。

```
npm install lerna -g
git clone https://gitee.com/ubml/farris-vue.git
cd farris-vue
lerna bootstrap
```

### 2.3 运行示例站点

执行以下命令。

```
cd packages/ui-vue
yarn run docs:dev
```

在浏览器中访问：`http://localhost:5173/` 查看示例页面。

![示例页面](./farris_home_page.png)

### 2.4 使用示例站点

你可以访问示例页面站点：

1. 点击「快速开始」了解如何安装使用 Farris Vue。
2. 点击「组件」体验组件视觉和交互效果，查看组件 API 说明文档。

## 3. 如何使用 Farris Vue

### 3.1 安装@farris/ui-vue

```
npm install @farris/ui-vue
```

```
yarn add @farris/ui-vue
```

### 3.2 在应用中引入 Farris Vue

在`main.ts`文件中引入`@farris/ui-vue`。

```ts
import { createApp } from 'vue';
import App from './App.vue';
import Farris from '@farris/ui-vue';

createApp(App).use(Farris).mount('#app');
```

### 3.3 在应用中使用 Farris Vue

在`App.vue`文件中使用 Farris Vue 组件。

```vue
<template>
    <f-button-edit></f-button-edit>
</template>
```

## 4. 如何参与贡献

还原你参与贡献 Farris Vue
在这里你可以参与以下贡献内容：

-   使用 `TypeScript` + `TSX` + `SystemJs` + `Rollup` 技术开发 Farris Vue 组件
-   参与贡献 Farris 主题工具
-   参与贡献 Farris 开发文档

如果你决定参与贡献 Farris Vue，可以先从阅读我们的[贡献指南](./CONTRIBUTING.md)开始。

## ✨ 贡献者

感谢以下 Farris Vue 的贡献者

<table>
  <!-- <tr>
    <td align="center"><a href="https://juejin.cn/user/712139267650141"><img src="https://foruda.gitee.com/avatar/1662181279170802242/610337_chenshj_1662181279.png!avatar60" width="100px;" alt=""/><br /><sub><b>陈圣杰</b></sub></a><br /></td>
  </tr> -->
</table>

## 开源许可

[Apache License 2.0](https://gitee.com/ubml/farris-vue/blob/master/LICENSE)
