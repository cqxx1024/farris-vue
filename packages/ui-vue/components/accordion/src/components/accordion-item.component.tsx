import { computed, defineComponent, ref, SetupContext } from 'vue';
import { accordionItemProps, AccordionItemProps } from './accordion-item.props';

import './accordion-item.scss';

export default defineComponent({
    name: 'FAccordionItem',
    props: accordionItemProps,
    emits: [],
    setup(props: AccordionItemProps, context: SetupContext) {
        const title = ref(props.title);
        const isActive = ref(false);
        const isDisabled = ref(false);

        function selectAccordionItem() {
            isActive.value = !isActive.value;
        }

        function onClick($event: Event) {
            selectAccordionItem();
        }

        const accordionItemClass = computed(() => ({
            'f-state-disable': isDisabled.value,
            card: true,
            'farris-panel-item': true,
            'f-state-selected': isActive.value
        }));

        const shouldShowHeader = computed(() => {
            return true;
        });

        const shouldShowCustomHeader = computed(() => {
            return false;
        });

        const headIconClass = computed(() => ({
            'f-icon': true,
            'f-accordion-collapse': !isActive.value,
            'f-accordion-expand': isActive.value
        }));

        const cardContainerStyle = computed(() => {
            const styleObject = {
                transition: 'height 0.36s ease 0s',
                height: isActive.value ? `${props.height}px` : 0
            };
            if (!isActive.value) {
                styleObject['overflow'] = 'hidden';
            }
            return styleObject;
        });

        return () => {
            return (
                <div class={accordionItemClass.value}>
                    <div class="card-header" onClick={onClick}>
                        <div class="panel-item-title">
                            {shouldShowHeader.value && <span>{title.value}</span>}
                            {shouldShowCustomHeader.value && context.slots.head && context.slots.head()}
                            <span class={headIconClass.value}></span>
                        </div>
                        <div class="panel-item-tool">{context.slots.toolbar && context.slots.toolbar()}</div>
                        <div class="panel-item-clear"></div>
                    </div>
                    <div style={cardContainerStyle.value}>
                        <div class="card-body">{context.slots.content && context.slots.content()}</div>
                    </div>
                </div>
            );
        };
    }
});
