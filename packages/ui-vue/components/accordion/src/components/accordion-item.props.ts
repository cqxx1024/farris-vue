import { ExtractPropTypes } from 'vue';

export const accordionItemProps = {
    width: { type: Number },
    height: { type: Number, default: 100 },
    title: { type: String, default: '' },
    disable: { type: Boolean, default: false }
};
export type AccordionItemProps = ExtractPropTypes<typeof accordionItemProps>;
