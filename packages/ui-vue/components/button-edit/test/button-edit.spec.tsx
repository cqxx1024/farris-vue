import { mount } from '@vue/test-utils';
import { ref } from 'vue';
import { ButtonEdit } from '..';

describe('f-button-edit', () => {
  const mocks = {};

  beforeAll(() => {});

  describe('properties', () => {
    it('should has default props', () => {});
    it('should has auto complete', () => {});
    it('should net has auto complete', () => {});
    it('should be disabled', () => {});
    it('should net be disabled', () => {});
    it('should be editable', () => {
      const wrapper = mount({
        setup(props, ctx) {
          return () => {
            return <ButtonEdit editable={true}></ButtonEdit>;
          };
        },
      });
      expect(wrapper.find('div').find('div').find('input').attributes['readonly']).toBeFalsy();
    });
    it('should not be editable', () => {
      const wrapper = mount({
        setup(props, ctx) {
          return () => {
            return <ButtonEdit editable={false}></ButtonEdit>;
          };
        },
      });
      expect(wrapper.find('.f-cmp-inputgroup').exists()).toBeTruthy();
      expect(wrapper.find('div').find('div').find('input').find('[readonly]').exists).toBeTruthy();
    });
    it('should show clear button', () => {});
    it('should not show clear button', () => {});
    it('should be readonly', () => {});
    it('should not be readonly', () => {});
    it('should enable text alignment', () => {});
    it('should show append button even be disabled', () => {});
    it('should has title', () => {});
    it('should has type', () => {});
    it('should has placeholder', () => {});
    it('should has min length', () => {});
    it('should has max length', () => {});
    it('should has tab index', () => {});
  });

  describe('render', () => {});

  describe('methods', () => {});

  describe('events', () => {
    it('should emit event named clear when click clear button', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit enableClear onClear={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('.input-group-clear').trigger('click');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named change when changed text box value', async () => {
      const handleClick = jest.fn();
      const num = ref('0');
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit v-model={num.value} onChange={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').setValue('test');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named click whend click text box', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onClick={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('click');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named clickButton when click append button', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onClickButton={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('.input-group-append-button').trigger('click');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named blur when text box lost focus', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onBlur={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('blur');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named focus when text box get focus', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onFocus={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('focus');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named mouseEnterIcon when mouse move in append button', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onMouseEnterIcon={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('.input-group-append-button').trigger('mouseenter');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named mouseLeaveIcon when mouse leave append button', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onMouseLeaveIcon={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('.input-group-append-button').trigger('mouseleave');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named keyup when input text in text box', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onKeyup={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('keyup');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named keydown when input text in text box', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onKeydown={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('keydown');
      expect(handleClick).toBeCalled();
    });

    it('should emit event named input when input text in text box', async () => {
      const handleClick = jest.fn();
      const wrapper = mount({
        setup() {
          return () => {
            return <ButtonEdit onInput={handleClick}></ButtonEdit>;
          };
        },
      });
      await wrapper.find('div').find('div').find('input').trigger('input');
      expect(handleClick).toBeCalled();
    });
  });

  describe('behaviors', () => {
    it('should hightlight text box when mouse in', () => {});
    it('should show clear button when mouse in text box', () => {});
    it('should show clear button when fouse text box and it not empty', () => {});
    it('should hide clear button when text box is emtyp', () => {});
    it('should show clear button when text any word from empty', () => {});
  });
});
