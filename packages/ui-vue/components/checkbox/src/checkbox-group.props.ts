import { ExtractPropTypes } from 'vue';
import { Checkbox } from './composition/types';

export const checkboxGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 组件名称
     */
    name: { type: String, default: '' },
    /**
     * 单选组枚举数组
     */
    enumData: Array<Checkbox>,
    /**
     * 枚举数组中展示文本的key值。
     */
    textField: { type: String, default: 'name' },
    /**
     * 枚举数组中枚举值的key值。
     */
    valueField: { type: String, default: 'value' },
    /**
     * 组件是否水平排列
     */
    horizontal: { type: Boolean, default: false },
    /**
     * 禁用组件，不允许切换单选值
     */
    disable: { type: Boolean, default: false },

    /**
     * 组件值，字符串或者数组
     */
    modelValue: [String, Array<string>],

    /**
     * 输入框Tab键索引
     */
    tabIndex: { type: Number, default: 0 },

    /**
     * 分隔符，默认逗号
     */
    separator: { type: String, default: ',' },

    /**
     * 值类型是否为字符串
     */
    isStringValue: { type: Boolean, default: true },

    /**
     * 异步获取枚举数组方法
     */
    // loadData: () => Observable < { data: Array<Checkbox> } >
    loadData: { type: Function }
};

export type CheckboxGroupProps = ExtractPropTypes<typeof checkboxGroupProps>;
