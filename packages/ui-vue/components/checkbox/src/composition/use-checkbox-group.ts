import { ChangeCheckbox, Checkbox } from './types';
import { computed, Ref, SetupContext, ref } from 'vue';
import { CheckboxGroupProps } from '../checkbox-group.props';

export function useCheckboxGroup(props: CheckboxGroupProps, context: SetupContext, modelValue: Ref<string>): ChangeCheckbox {

    const canChangeRadioButton = computed(() => !props.disable);
    const enumData = ref(props.enumData || []);

    function getValue(item: Checkbox): any {
        return item[props.valueField];
    };

    function getText(item: Checkbox): any {
        return item[props.textField];
    };

    /**
     * 值到数组值的转换
     */
    function transformToArr(value: any): string[] {
        if (!value) {
            return [];
        }
        if (!props.isStringValue) {
            return value;
        }

        return value.split(props.separator);
    }

    /**
     * 值到字符串值的转换
     */
    function transformToStr(value: Array<string>) {

        const allVals = enumData.value.map(n => getValue(n));
        const r = [];
        allVals.forEach(n => {
            if (value.some(item => item === n)) {
                r.push(n);
            }
        });

        if (!props.isStringValue) {
            return r;
        }
        return r.join(props.separator);
    }
    /**
     * 校验复选框是否为选中状态
     */
    function checked(item: Checkbox) {
        const val = String(getValue(item));
        const checkedValue = transformToArr(modelValue.value);

        // 多值
        return checkedValue.some(item => item === val);
    }

    /**
     * 点击复选框事件
     */
    function onClickCheckbox(item: Checkbox, $event: Event) {
        if (canChangeRadioButton.value) {
            let arrValue = transformToArr(modelValue.value) || [];

            const val = String(getValue(item));
            if (!arrValue || !arrValue.length) {
                arrValue.push(val);
            } else if (arrValue.some(item => item === val)) {
                arrValue = arrValue.filter(n => n !== val);
            } else {
                arrValue.push(val);
            }

            // 更新value值
            modelValue.value = transformToStr(arrValue);

            context.emit('changeValue', modelValue.value);

            // 不可少，用于更新组件绑定值
            context.emit('update:modelValue', modelValue.value);

        }

        $event.stopPropagation();
    }


    function loadData() {
        if (props.loadData) {
            props.loadData().subscribe(res => {
                enumData.value = res.data || [];
            });
        }
    }
    return {
        enumData,

        getValue,
        getText,
        checked,
        onClickCheckbox,
        loadData
    };
}
