import { defineComponent, SetupContext } from 'vue';
import { optionsProps, OptionsProps } from '../combo-list.props';
import { IOption } from '../types';
import FOption from '../components/option';

export default defineComponent({
    name: 'FOptions',
    props: optionsProps,
    emits: [],
    inheritAttrs: false,
    setup(props: OptionsProps, context: SetupContext) {
        return () => {
            return (
                <ul class="list-group list-group-flush">
                    {props?.options?.map((option: IOption) => (
                        <FOption value={option.value} name={option.name} disabled={option.disabled}></FOption>
                    ))}
                </ul>
            );
        };
    }
});