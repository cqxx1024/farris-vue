import { ref } from 'vue';
import { IUseState } from '../types';

/**
 * state hook
 * @param initial 初始值
 * @returns [state,action]
 */
export function useState<T = any>(initial: T): IUseState<T> {
    if (typeof initial === 'undefined') {
        throw new Error('invalid initial: initial must have value.');
    }
    const state = ref(initial);
    const action = (value: any) => {
        state.value = value;
    };
    return [state, action];
}