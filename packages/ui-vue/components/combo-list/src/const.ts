import { InjectionKey } from "vue";
import { ComboListContext } from "./types";

export const groupIcon = '<span class="f-icon f-icon-arrow-60-down"></span>';
/**
 * combo list injector token
 */
export const COMBO_LIST_TOKEN: InjectionKey<ComboListContext> = Symbol('fComboList');