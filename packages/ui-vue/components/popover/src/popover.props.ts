import { ExtractPropTypes, PropType } from 'vue';

export type PopoverPlacement = 'top' | 'bottom' | 'left' | 'right' | 'auto';

export const popoverProps = {
    title: { type: String },
    placement: { type: String as PropType<PopoverPlacement>, default: 'top' },
    reference: {
        type: Object as PropType<HTMLElement>
    }
};

export type PopoverProps = ExtractPropTypes<typeof popoverProps>;
