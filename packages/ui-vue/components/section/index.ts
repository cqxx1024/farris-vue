import type { App } from 'vue';
import Section from './src/section.component';

export * from './src/section.props';
export { Section };

export default {
    install(app: App): void {
        app.component(Section.name, Section);
    }
};
