import { computed, defineComponent, ref, SetupContext } from 'vue';
import { ButtonConfig, SectionProps, sectionProps } from './section.props';

export default defineComponent({
    name: 'FSection',
    props: sectionProps,
    emits: [],
    setup(props: SectionProps, context: SetupContext) {
        const shouldShowHeader = computed(() => {
            return true;
        });

        const shouldShowHeaderTitle = computed(() => {
            return true;
        });

        const shouldShowSubHeaderTitle = computed(() => {
            return true;
        });

        const shouldShowToolbarInHeader = computed(() => {
            return false;
        });

        const shouldShowToolbarInContent = computed(() => {
            return false;
        });

        const shouldShowToolbarTemplateInContent = computed(() => {
            return false;
        });

        const shouldShowExtendArea = computed(() => {
            return false;
        });

        const toolbarButtons = ref([]);

        const headerClass = computed(() => {
            const customClassArray = props.headerClass.split(' ');
            const headClassObject = {
                'f-section-header': true
            };
            customClassArray.reduce<Record<string, unknown>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, headClassObject);
            return headClassObject;
        });

        const extendAreaClass = computed(() => ({
            'f-section-extend': true
        }));

        const contentClass = computed(() => {
            const customClassArray = props.contentClass.split(' ');
            const contentClassObject = {
                'f-section-content': true
            };
            customClassArray.reduce<any>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, contentClassObject);
            return contentClassObject;
        });

        function getToolbarState(buttonId: string, visiableMap: any, defaultValue: boolean) {
            return true;
        }

        const mainTitle = ref(props.mainTitle);

        const subTitle = ref(props.subTitle);

        function renderSectionHeader() {
            return (
                shouldShowHeader.value && (
                    <div class={headerClass.value}>
                        {shouldShowHeaderTitle.value && (
                            <div class="f-title">
                                <h4 class="f-title-text">{mainTitle.value}</h4>
                                {shouldShowSubHeaderTitle.value && <span>{subTitle.value}</span>}
                            </div>
                        )}
                    </div>
                )
            );
        }

        function renderSectionContent() {
            return <div class={contentClass.value}>{context.slots.default && context.slots.default()}</div>;
        }

        return () => {
            return (
                <div class="f-section">
                    {renderSectionHeader()}
                    {renderSectionContent()}
                </div>
            );
        };
    }
});
