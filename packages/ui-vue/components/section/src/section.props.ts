import { ExtractPropTypes, PropType } from 'vue';

export interface ButtonAppearance {
    class: string;
}

export interface ButtonConfig {
    id: string;
    disable: boolean;
    title: string;
    click: any;
    appearance: ButtonAppearance;
    visible?: boolean;
}

export interface ToolbarConfig {
    position: string;
    contents: ButtonConfig[];
}

export const sectionProps = {
    contentClass: { type: String, default: '' },
    maxStatus: { type: Boolean, default: false },
    enableAccording: { type: Boolean, default: false },
    enableCollapse: { type: Boolean, default: true },
    mainTitle: { type: String, default: '' },
    subTitle: { type: String, default: '' },
    showHeader: { type: Boolean, default: true },
    enableMaximize: { type: Boolean, default: false },
    fill: { type: Boolean, default: false },
    expandStatus: { type: Boolean, default: true },
    cotext: { type: Object },
    index: { type: Number },
    toolbarPosition: { type: String, default: '' },
    toolbarButtons: { type: Array<object>, default: [] },
    toolbar: { type: Object as PropType<ToolbarConfig>, default: {} },
    showToolbarMoreButton: { type: Boolean, default: true },
    clickThrottleTime: { type: Number, default: 350 },
    headerClass: { type: String, default: '' }
};
export type SectionProps = ExtractPropTypes<typeof sectionProps>;
