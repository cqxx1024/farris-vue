import type { App } from 'vue';
import Text from './src/text.component';

export * from './src/text.props';
export { Text };

export default {
    install(app: App): void {
        app.component(Text.name, Text);
    }
};
