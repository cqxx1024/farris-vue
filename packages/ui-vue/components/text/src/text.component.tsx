import { computed, defineComponent, ref, SetupContext } from 'vue';
import { TextProps, textProps } from './text.props';

export default defineComponent({
    name: 'FText',
    props: textProps,
    emits: ['update:modelValue',],
    setup(props: TextProps, context: SetupContext) {
        const isTextArea = ref(true);
        const autoSize = ref(true);
        const textAlginment = ref('');
        const height = ref(0);
        const maxHeight = ref(0);
        const modelValue = ref(props.modelValue);
        const textClass = computed(() => ({
            'f-form-control-text': !isTextArea.value,
            'f-form-context-textarea': isTextArea,
            'f-component-text-auto-size': autoSize.value
        }));

        const textStyle = computed(() => ({
            textalign: textAlginment.value,
            height: !autoSize.value && height.value > 0 ? `${height.value}px` : '',
            'min-height': !autoSize.value && height.value > 0 ? `${height.value}px` : '',
            'max-height': !autoSize.value && maxHeight.value > 0 ? `${maxHeight.value}px` : ''
        }));

        const text = computed(() => {
            // text && text.length > 0 ?  text : control
            return '';
        });

        return () => {
            return (
                <span class={textClass.value} style={textStyle.value}>
                    {modelValue.value}
                </span>
            );
        };
    }
});
