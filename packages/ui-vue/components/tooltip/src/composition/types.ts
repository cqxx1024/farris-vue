import { ComputedRef } from "vue";
import { TooltipPlacement } from "../tooltip.props";

export type RectDirection = 'top' | 'bottom' | 'right' | 'left';
export type RectSizeProperty = 'height' | 'width';
export interface RectPosition {
    top: number;
    left: number;
    right?: number;
}
export interface TooltipPosition {
    arrow: RectPosition;
    tooltip: RectPosition;
}

export interface UseTooltipPosition {
    tooltipPlacement: ComputedRef<TooltipPlacement>;
    tooltipPosition: ComputedRef<RectPosition>;
    arrowPosition: ComputedRef<RectPosition>;
}

export interface UseCalculatePosition {
    calculate: (
        placementAndAlignment: TooltipPlacement,
        hostBound: DOMRect,
        tooltipBound: DOMRect,
        tooltipContentBound: DOMRect,
        arrowBound: DOMRect
    ) => TooltipPosition;
}

export interface UseRelative {
    getRelativeElementBound: () => DOMRect;
}

export interface UseAdjustPlacement {
    adjustPlacement: (
        placementAndAlignment: TooltipPlacement,
        referenceBoundingRect: DOMRect,
        arrowReferenceBoundingRect: DOMRect,
        tooltipBound: DOMRect,
        arrowBound: DOMRect
    ) => TooltipPlacement;
}

export interface UseAdjustPosition {
    adjustPosition: (
        placementAndAlignment: TooltipPlacement,
        originalPosition: RectPosition,
        relativeElementRect: DOMRect,
        tooltipRect: DOMRect
    ) => RectPosition;
}
