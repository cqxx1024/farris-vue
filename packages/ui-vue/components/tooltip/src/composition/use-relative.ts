import { ref, SetupContext } from "vue";
import { TooltipProps } from "../tooltip.props";
import { UseRelative } from "./types";

export function useRelative(props: TooltipProps, context: SetupContext): UseRelative {

    const horizontalRelativeElement = ref(props.horizontalRelative);

    const verticalRelativeElement = ref(props.verticalRelative);

    /**
     * 获取纠正元素
     */
    function getRelativeElement(relativeElement: any) {
        if (relativeElement instanceof HTMLElement) {
            return relativeElement;
        }
        if (typeof relativeElement == 'string') {
            return document.querySelector(relativeElement as string);
        }
        return relativeElement;
    }

    /**
    * 确认参照的边界
    */
    function getRelativeElementBound(): DOMRect {
        let right = document.documentElement.clientWidth;
        let bottom = document.documentElement.clientHeight;
        let top = 0;
        let left = 0;
        let x = 0;
        let y = 0;
        let height = bottom - top;
        let width = right - left;
        // 横向参照
        if (horizontalRelativeElement.value) {
            const rectifyReferenceHEl = getRelativeElement(horizontalRelativeElement.value);
            ({ left, right, x, width } = rectifyReferenceHEl.getBoundingClientRect());
        }
        // 纵向参照
        if (verticalRelativeElement.value) {
            const rectifyReferenceVEl = getRelativeElement(verticalRelativeElement.value);
            ({ bottom, top, y, height } = rectifyReferenceVEl.getBoundingClientRect());
        }
        return { top, left, right, bottom, height, width, x, y } as DOMRect;
    }

    return { getRelativeElementBound };
}
