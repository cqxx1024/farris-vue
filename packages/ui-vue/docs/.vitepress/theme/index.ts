import Accordion from '../../../components/accordion';
import Avatar from '../../../components/avatar';
import Button from '../../../components/button';
import ButtonEdit from '../../../components/button-edit';
import ComboList from '../../../components/combo-list';
import Notify from '../../../components/notify';
import Popover from '../../../components/popover';
import RadioGroup from '../../../components/radio-group';
import Section from '../../../components/section';
import Switch from '../../../components/switch';
import Tabs from '../../../components/tabs';
import Text from '../../../components/text';
import Tooltip from '../../../components/tooltip';
import FarrisTheme from '../farris-theme';
import 'vitepress-theme-demoblock/theme/styles/index.css';
import { registerComponents } from './register-components.js';
import { insertBaiduScript } from './insert-baidu-script'

import '../../../public/assets/farris-all.css';

export default {
    ...FarrisTheme,
    enhanceApp({ app }) {
        app.use(Accordion)
            .use(Avatar)
            .use(Button)
            .use(ButtonEdit)
            .use(ComboList)
            .use(Notify)
            .use(Popover)
            .use(RadioGroup)
            .use(Section)
            .use(Switch)
            .use(Tabs)
            .use(Text)
            .use(Tooltip);
        registerComponents(app);
        insertBaiduScript()
    }
};
