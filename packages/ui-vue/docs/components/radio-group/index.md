# Radio Group 单选组

Radio Group 是一个单选一组枚举值的输入组件。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const radioEnumData = ref([
    { value: 'first', name: 'Fist Item' },
    { value: 'second', name: 'Second Item' },
    { value: 'third', name: 'Third Item' }
]);
const value = ref('first');
</script>
<template>
    <f-radio-group :enum-data="radioEnumData" v-model="value"> </f-radio-group>
    <div class="my-2">
        <span>当前选中值: {{ value }}</span>
    </div>
</template>
```

:::

## 禁用状态

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const disable = ref(true);
const radioEnumData = ref([
    { value: 'first', name: 'Fist Item' },
    { value: 'second', name: 'Second Item' },
    { value: 'third', name: 'Third Item' }
]);
const value = ref('first');
</script>
<template>
    <label for="radio_disabled" class="mr-1">禁用单选组</label>
    <input type="checkbox" id="radio_disabled" v-model="disable" />
    <br />
    <f-radio-group :disabled="disable" :enum-data="radioEnumData" v-model="value"> </f-radio-group>
    <div class="my-2">
        <span>当前选中值: {{ value }}</span>
    </div>
</template>
```

:::

## 排列方式

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const radioEnumData = ref([
    { value: 'first', name: 'Fist Item' },
    { value: 'second', name: 'Second Item' },
    { value: 'third', name: 'Third Item' }
]);
const value = ref('first');
</script>
<template>
    <div class="my-2">
        <span>纵向排列:</span>
    </div>
    <f-radio-group :enum-data="radioEnumData" v-model="value"> </f-radio-group>
    <div class="my-2">
        <span>横向排列</span>
    </div>
    <f-radio-group :enum-data="radioEnumData" v-model="value" :horizontal="true"> </f-radio-group>
</template>
```

:::

## 类型

```typescript
export interface Radio {
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeRadio {
    enumData: ComputedRef<Array<Radio>>;

    /**
     * 获取枚举值
     */
    getValue(item: Radio): any;
    /**
     * 获取枚举文本
     */
    getText(item: Radio): any;

    /**
     * 切换单选按钮事件
     */
    onClickRadio: (item: Radio, $event: Event) => void;
}
```

## 属性

| 属性名     | 类型                  | 默认值  | 说明               |
| :--------- | :-------------------- | :------ | :----------------- |
| id         | `string`              | --      | 组件标识           |
| name       | `string`              | --      | 组件名称           |
| enumData   | `object[] as Radio[]` | []      | 单选组数据源       |
| textField  | `string`              | 'name'  | 显示文本字段       |
| valueField | `string`              | 'value' | 值字段             |
| horizontal | `boolean`             | false   | 是否横向显示枚举项 |
| disabled   | `boolean`             | false   | 是否禁用组件       |
| tabIndex   | `number`              | --      | Tab 键索引         |

## 插槽

::: tip
暂无内容
:::
